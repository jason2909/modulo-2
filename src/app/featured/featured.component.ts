import { Component, OnInit } from "@angular/core";

import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as dialogs from "tns-core-modules/ui/dialogs";

@Component({
    selector: "Featured",
    templateUrl: "./featured.component.html"
})
export class FeaturedComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

 dolater(fn) { setTimeout(fn, 1000);}

    ngOnInit(): void {

      this.dolater(() =>
       dialogs.action("Mensaje", "cancelar!", ["option1","Option2"])
       .then((result) => {
          console.log("resultado:" + result);
          if(result == "option1"){
              this.dolater(() =>
              dialogs.alert({
                title: "Titulo 1",
                message: " hi 1",

              }).then(() => console.log("cerrado 1!")));
          }else if(result == "option2"){
            this.dolater(() =>
            dialogs.alert({
              title: "Titulo 2",
              message: " hi 2",

          }).then(() => console.log("cerrado 2!")));
          }
      }));

    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
