import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import {NoticiasService} from "../domain/Noticias.Service";
@Component({
    selector: "Search",
    templateUrl: "./search.component.html"
  

})
export class SearchComponent implements OnInit {

    constructor( private noticias: NoticiasService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
      this.noticias.agregar("hola"!);
      this.noticias.agregar("hola1"!);
      this.noticias.agregar("hola2"!);
      this.noticias.agregar("hola3"!);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
